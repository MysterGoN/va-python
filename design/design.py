# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setMinimumSize(QtCore.QSize(800, 600))
        MainWindow.setMaximumSize(QtCore.QSize(800, 600))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 801, 601))
        self.tabWidget.setObjectName("tabWidget")
        self.selectT = QtWidgets.QWidget()
        self.selectT.setObjectName("selectT")
        self.btnSearch = QtWidgets.QPushButton(self.selectT)
        self.btnSearch.setGeometry(QtCore.QRect(580, 60, 201, 23))
        self.btnSearch.setObjectName("btnSearch")
        self.vacationSelectTable = QtWidgets.QTableWidget(self.selectT)
        self.vacationSelectTable.setGeometry(QtCore.QRect(0, 0, 561, 571))
        self.vacationSelectTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.vacationSelectTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.vacationSelectTable.setObjectName("vacationSelectTable")
        self.vacationSelectTable.setColumnCount(0)
        self.vacationSelectTable.setRowCount(0)
        self.vacationSelectTable.horizontalHeader().setSortIndicatorShown(True)
        self.vacationSelectTable.verticalHeader().setSortIndicatorShown(False)
        self.cbMonth = QtWidgets.QComboBox(self.selectT)
        self.cbMonth.setGeometry(QtCore.QRect(580, 30, 201, 22))
        self.cbMonth.setObjectName("cbMonth")
        self.label_21 = QtWidgets.QLabel(self.selectT)
        self.label_21.setGeometry(QtCore.QRect(580, 10, 47, 13))
        self.label_21.setObjectName("label_21")
        self.tabWidget.addTab(self.selectT, "")
        self.adminT = QtWidgets.QWidget()
        self.adminT.setObjectName("adminT")
        self.leadirshipT = QtWidgets.QTabWidget(self.adminT)
        self.leadirshipT.setGeometry(QtCore.QRect(0, 0, 801, 581))
        self.leadirshipT.setObjectName("leadirshipT")
        self.vacationT = QtWidgets.QWidget()
        self.vacationT.setObjectName("vacationT")
        self.vacationTable = QtWidgets.QTableWidget(self.vacationT)
        self.vacationTable.setGeometry(QtCore.QRect(0, 0, 541, 551))
        self.vacationTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.vacationTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.vacationTable.setObjectName("vacationTable")
        self.vacationTable.setColumnCount(0)
        self.vacationTable.setRowCount(0)
        self.toolBox_2 = QtWidgets.QToolBox(self.vacationT)
        self.toolBox_2.setGeometry(QtCore.QRect(550, 40, 231, 501))
        self.toolBox_2.setObjectName("toolBox_2")
        self.page_4 = QtWidgets.QWidget()
        self.page_4.setGeometry(QtCore.QRect(0, 0, 231, 420))
        self.page_4.setObjectName("page_4")
        self.groupBox_16 = QtWidgets.QGroupBox(self.page_4)
        self.groupBox_16.setGeometry(QtCore.QRect(0, 0, 231, 371))
        self.groupBox_16.setObjectName("groupBox_16")
        self.btnAddVacation = QtWidgets.QPushButton(self.groupBox_16)
        self.btnAddVacation.setGeometry(QtCore.QRect(10, 340, 211, 23))
        self.btnAddVacation.setObjectName("btnAddVacation")
        self.label_11 = QtWidgets.QLabel(self.groupBox_16)
        self.label_11.setGeometry(QtCore.QRect(10, 20, 51, 16))
        self.label_11.setObjectName("label_11")
        self.deVacationSheduleAdd = QtWidgets.QDateEdit(self.groupBox_16)
        self.deVacationSheduleAdd.setGeometry(QtCore.QRect(10, 90, 211, 22))
        self.deVacationSheduleAdd.setObjectName("deVacationSheduleAdd")
        self.label_12 = QtWidgets.QLabel(self.groupBox_16)
        self.label_12.setGeometry(QtCore.QRect(10, 70, 121, 16))
        self.label_12.setObjectName("label_12")
        self.cbVacationWorkerAdd = QtWidgets.QComboBox(self.groupBox_16)
        self.cbVacationWorkerAdd.setGeometry(QtCore.QRect(10, 40, 211, 22))
        self.cbVacationWorkerAdd.setObjectName("cbVacationWorkerAdd")
        self.label_13 = QtWidgets.QLabel(self.groupBox_16)
        self.label_13.setGeometry(QtCore.QRect(10, 120, 101, 16))
        self.label_13.setObjectName("label_13")
        self.label_14 = QtWidgets.QLabel(self.groupBox_16)
        self.label_14.setGeometry(QtCore.QRect(10, 170, 171, 16))
        self.label_14.setObjectName("label_14")
        self.label_15 = QtWidgets.QLabel(self.groupBox_16)
        self.label_15.setGeometry(QtCore.QRect(10, 210, 81, 16))
        self.label_15.setObjectName("label_15")
        self.deVacationActualAdd = QtWidgets.QDateEdit(self.groupBox_16)
        self.deVacationActualAdd.setGeometry(QtCore.QRect(10, 140, 211, 22))
        self.deVacationActualAdd.setWrapping(False)
        self.deVacationActualAdd.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.deVacationActualAdd.setAccelerated(False)
        self.deVacationActualAdd.setProperty("showGroupSeparator", False)
        self.deVacationActualAdd.setCurrentSection(QtWidgets.QDateTimeEdit.DaySection)
        self.deVacationActualAdd.setCalendarPopup(False)
        self.deVacationActualAdd.setObjectName("deVacationActualAdd")
        self.vacationNumberOfDaysAdd = QtWidgets.QLineEdit(self.groupBox_16)
        self.vacationNumberOfDaysAdd.setGeometry(QtCore.QRect(10, 190, 201, 20))
        self.vacationNumberOfDaysAdd.setObjectName("vacationNumberOfDaysAdd")
        self.lwVacationAdd = QtWidgets.QListWidget(self.groupBox_16)
        self.lwVacationAdd.setGeometry(QtCore.QRect(10, 230, 211, 101))
        self.lwVacationAdd.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.lwVacationAdd.setObjectName("lwVacationAdd")
        self.toolBox_2.addItem(self.page_4, "")
        self.page_5 = QtWidgets.QWidget()
        self.page_5.setGeometry(QtCore.QRect(0, 0, 100, 30))
        self.page_5.setObjectName("page_5")
        self.groupBox_17 = QtWidgets.QGroupBox(self.page_5)
        self.groupBox_17.setGeometry(QtCore.QRect(0, 0, 231, 401))
        self.groupBox_17.setObjectName("groupBox_17")
        self.cbVacationUpdate = QtWidgets.QComboBox(self.groupBox_17)
        self.cbVacationUpdate.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbVacationUpdate.setObjectName("cbVacationUpdate")
        self.btnUpdateVacation = QtWidgets.QPushButton(self.groupBox_17)
        self.btnUpdateVacation.setGeometry(QtCore.QRect(10, 370, 211, 23))
        self.btnUpdateVacation.setObjectName("btnUpdateVacation")
        self.label_20 = QtWidgets.QLabel(self.groupBox_17)
        self.label_20.setGeometry(QtCore.QRect(10, 100, 121, 16))
        self.label_20.setObjectName("label_20")
        self.deVacationSheduleUpdate = QtWidgets.QDateEdit(self.groupBox_17)
        self.deVacationSheduleUpdate.setGeometry(QtCore.QRect(10, 120, 211, 22))
        self.deVacationSheduleUpdate.setObjectName("deVacationSheduleUpdate")
        self.deVacationActualUpdate = QtWidgets.QDateEdit(self.groupBox_17)
        self.deVacationActualUpdate.setGeometry(QtCore.QRect(10, 170, 211, 22))
        self.deVacationActualUpdate.setObjectName("deVacationActualUpdate")
        self.cbVacationWorkerUpdate = QtWidgets.QComboBox(self.groupBox_17)
        self.cbVacationWorkerUpdate.setGeometry(QtCore.QRect(10, 70, 211, 22))
        self.cbVacationWorkerUpdate.setObjectName("cbVacationWorkerUpdate")
        self.label_19 = QtWidgets.QLabel(self.groupBox_17)
        self.label_19.setGeometry(QtCore.QRect(10, 240, 81, 16))
        self.label_19.setObjectName("label_19")
        self.label_18 = QtWidgets.QLabel(self.groupBox_17)
        self.label_18.setGeometry(QtCore.QRect(10, 200, 171, 16))
        self.label_18.setObjectName("label_18")
        self.label_16 = QtWidgets.QLabel(self.groupBox_17)
        self.label_16.setGeometry(QtCore.QRect(10, 50, 51, 16))
        self.label_16.setObjectName("label_16")
        self.label_17 = QtWidgets.QLabel(self.groupBox_17)
        self.label_17.setGeometry(QtCore.QRect(10, 150, 101, 16))
        self.label_17.setObjectName("label_17")
        self.vacationNumberOfDaysUpdate = QtWidgets.QLineEdit(self.groupBox_17)
        self.vacationNumberOfDaysUpdate.setGeometry(QtCore.QRect(10, 220, 201, 20))
        self.vacationNumberOfDaysUpdate.setObjectName("vacationNumberOfDaysUpdate")
        self.lwVacationUpdate = QtWidgets.QListWidget(self.groupBox_17)
        self.lwVacationUpdate.setGeometry(QtCore.QRect(10, 260, 211, 101))
        self.lwVacationUpdate.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.lwVacationUpdate.setObjectName("lwVacationUpdate")
        self.toolBox_2.addItem(self.page_5, "")
        self.page_6 = QtWidgets.QWidget()
        self.page_6.setGeometry(QtCore.QRect(0, 0, 100, 30))
        self.page_6.setObjectName("page_6")
        self.groupBox_18 = QtWidgets.QGroupBox(self.page_6)
        self.groupBox_18.setGeometry(QtCore.QRect(0, 0, 231, 80))
        self.groupBox_18.setObjectName("groupBox_18")
        self.cbVacationDelete = QtWidgets.QComboBox(self.groupBox_18)
        self.cbVacationDelete.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbVacationDelete.setObjectName("cbVacationDelete")
        self.btnDeleteVacation = QtWidgets.QPushButton(self.groupBox_18)
        self.btnDeleteVacation.setGeometry(QtCore.QRect(10, 50, 211, 23))
        self.btnDeleteVacation.setObjectName("btnDeleteVacation")
        self.toolBox_2.addItem(self.page_6, "")
        self.btnUpdateVacationTable = QtWidgets.QPushButton(self.vacationT)
        self.btnUpdateVacationTable.setGeometry(QtCore.QRect(560, 10, 211, 23))
        self.btnUpdateVacationTable.setObjectName("btnUpdateVacationTable")
        self.leadirshipT.addTab(self.vacationT, "")
        self.workerT = QtWidgets.QWidget()
        self.workerT.setObjectName("workerT")
        self.workerTable = QtWidgets.QTableWidget(self.workerT)
        self.workerTable.setGeometry(QtCore.QRect(0, 0, 541, 551))
        self.workerTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.workerTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.workerTable.setObjectName("workerTable")
        self.workerTable.setColumnCount(0)
        self.workerTable.setRowCount(0)
        self.btnUpdateWorkerTable = QtWidgets.QPushButton(self.workerT)
        self.btnUpdateWorkerTable.setGeometry(QtCore.QRect(560, 10, 211, 23))
        self.btnUpdateWorkerTable.setObjectName("btnUpdateWorkerTable")
        self.toolBox = QtWidgets.QToolBox(self.workerT)
        self.toolBox.setGeometry(QtCore.QRect(550, 40, 231, 411))
        self.toolBox.setObjectName("toolBox")
        self.page = QtWidgets.QWidget()
        self.page.setGeometry(QtCore.QRect(0, 0, 231, 330))
        self.page.setObjectName("page")
        self.groupBox_15 = QtWidgets.QGroupBox(self.page)
        self.groupBox_15.setGeometry(QtCore.QRect(0, 0, 231, 291))
        self.groupBox_15.setObjectName("groupBox_15")
        self.btnAddWorker = QtWidgets.QPushButton(self.groupBox_15)
        self.btnAddWorker.setGeometry(QtCore.QRect(10, 260, 211, 23))
        self.btnAddWorker.setObjectName("btnAddWorker")
        self.workerNameAdd = QtWidgets.QLineEdit(self.groupBox_15)
        self.workerNameAdd.setGeometry(QtCore.QRect(10, 40, 211, 20))
        self.workerNameAdd.setObjectName("workerNameAdd")
        self.label = QtWidgets.QLabel(self.groupBox_15)
        self.label.setGeometry(QtCore.QRect(10, 20, 31, 16))
        self.label.setObjectName("label")
        self.deWorkerAdd = QtWidgets.QDateEdit(self.groupBox_15)
        self.deWorkerAdd.setGeometry(QtCore.QRect(10, 80, 211, 22))
        self.deWorkerAdd.setObjectName("deWorkerAdd")
        self.label_2 = QtWidgets.QLabel(self.groupBox_15)
        self.label_2.setGeometry(QtCore.QRect(10, 60, 101, 16))
        self.label_2.setObjectName("label_2")
        self.cbWorkerAdministrationAdd = QtWidgets.QComboBox(self.groupBox_15)
        self.cbWorkerAdministrationAdd.setGeometry(QtCore.QRect(10, 130, 211, 22))
        self.cbWorkerAdministrationAdd.setObjectName("cbWorkerAdministrationAdd")
        self.label_3 = QtWidgets.QLabel(self.groupBox_15)
        self.label_3.setGeometry(QtCore.QRect(10, 110, 81, 16))
        self.label_3.setObjectName("label_3")
        self.cbWorkerDepartmentAdd = QtWidgets.QComboBox(self.groupBox_15)
        self.cbWorkerDepartmentAdd.setGeometry(QtCore.QRect(10, 180, 211, 22))
        self.cbWorkerDepartmentAdd.setObjectName("cbWorkerDepartmentAdd")
        self.label_4 = QtWidgets.QLabel(self.groupBox_15)
        self.label_4.setGeometry(QtCore.QRect(10, 160, 81, 16))
        self.label_4.setObjectName("label_4")
        self.cbWorkerLeadershipAdd = QtWidgets.QComboBox(self.groupBox_15)
        self.cbWorkerLeadershipAdd.setGeometry(QtCore.QRect(10, 230, 211, 22))
        self.cbWorkerLeadershipAdd.setObjectName("cbWorkerLeadershipAdd")
        self.label_5 = QtWidgets.QLabel(self.groupBox_15)
        self.label_5.setGeometry(QtCore.QRect(10, 210, 81, 16))
        self.label_5.setObjectName("label_5")
        self.toolBox.addItem(self.page, "")
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setGeometry(QtCore.QRect(0, 0, 100, 30))
        self.page_2.setObjectName("page_2")
        self.groupBox_14 = QtWidgets.QGroupBox(self.page_2)
        self.groupBox_14.setGeometry(QtCore.QRect(0, 0, 231, 321))
        self.groupBox_14.setObjectName("groupBox_14")
        self.cbWorkerUpdate = QtWidgets.QComboBox(self.groupBox_14)
        self.cbWorkerUpdate.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbWorkerUpdate.setObjectName("cbWorkerUpdate")
        self.label_6 = QtWidgets.QLabel(self.groupBox_14)
        self.label_6.setGeometry(QtCore.QRect(10, 50, 31, 16))
        self.label_6.setObjectName("label_6")
        self.cbWorkerLeadershipUpdate = QtWidgets.QComboBox(self.groupBox_14)
        self.cbWorkerLeadershipUpdate.setGeometry(QtCore.QRect(10, 260, 211, 22))
        self.cbWorkerLeadershipUpdate.setObjectName("cbWorkerLeadershipUpdate")
        self.label_7 = QtWidgets.QLabel(self.groupBox_14)
        self.label_7.setGeometry(QtCore.QRect(10, 90, 81, 16))
        self.label_7.setObjectName("label_7")
        self.cbWorkerAdministrationUpdate = QtWidgets.QComboBox(self.groupBox_14)
        self.cbWorkerAdministrationUpdate.setGeometry(QtCore.QRect(10, 160, 211, 22))
        self.cbWorkerAdministrationUpdate.setObjectName("cbWorkerAdministrationUpdate")
        self.deWorkerUpdate = QtWidgets.QDateEdit(self.groupBox_14)
        self.deWorkerUpdate.setGeometry(QtCore.QRect(10, 110, 211, 22))
        self.deWorkerUpdate.setObjectName("deWorkerUpdate")
        self.workerNameUpdate = QtWidgets.QLineEdit(self.groupBox_14)
        self.workerNameUpdate.setGeometry(QtCore.QRect(10, 70, 211, 20))
        self.workerNameUpdate.setObjectName("workerNameUpdate")
        self.cbWorkerDepartmentUpdate = QtWidgets.QComboBox(self.groupBox_14)
        self.cbWorkerDepartmentUpdate.setGeometry(QtCore.QRect(10, 210, 211, 22))
        self.cbWorkerDepartmentUpdate.setObjectName("cbWorkerDepartmentUpdate")
        self.btnUpdateWorker = QtWidgets.QPushButton(self.groupBox_14)
        self.btnUpdateWorker.setGeometry(QtCore.QRect(10, 290, 211, 23))
        self.btnUpdateWorker.setObjectName("btnUpdateWorker")
        self.label_8 = QtWidgets.QLabel(self.groupBox_14)
        self.label_8.setGeometry(QtCore.QRect(10, 240, 81, 16))
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.groupBox_14)
        self.label_9.setGeometry(QtCore.QRect(10, 140, 81, 16))
        self.label_9.setObjectName("label_9")
        self.label_10 = QtWidgets.QLabel(self.groupBox_14)
        self.label_10.setGeometry(QtCore.QRect(10, 190, 81, 16))
        self.label_10.setObjectName("label_10")
        self.toolBox.addItem(self.page_2, "")
        self.page_3 = QtWidgets.QWidget()
        self.page_3.setGeometry(QtCore.QRect(0, 0, 100, 30))
        self.page_3.setObjectName("page_3")
        self.groupBox_13 = QtWidgets.QGroupBox(self.page_3)
        self.groupBox_13.setGeometry(QtCore.QRect(0, 0, 231, 80))
        self.groupBox_13.setObjectName("groupBox_13")
        self.cbWorkerDelete = QtWidgets.QComboBox(self.groupBox_13)
        self.cbWorkerDelete.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbWorkerDelete.setObjectName("cbWorkerDelete")
        self.btnDeleteWorker = QtWidgets.QPushButton(self.groupBox_13)
        self.btnDeleteWorker.setGeometry(QtCore.QRect(10, 50, 211, 23))
        self.btnDeleteWorker.setObjectName("btnDeleteWorker")
        self.toolBox.addItem(self.page_3, "")
        self.btnWorkerUpPos = QtWidgets.QPushButton(self.workerT)
        self.btnWorkerUpPos.setGeometry(QtCore.QRect(550, 470, 51, 31))
        self.btnWorkerUpPos.setObjectName("btnWorkerUpPos")
        self.btnWorkerDownPos = QtWidgets.QPushButton(self.workerT)
        self.btnWorkerDownPos.setGeometry(QtCore.QRect(550, 510, 51, 31))
        self.btnWorkerDownPos.setObjectName("btnWorkerDownPos")
        self.leadirshipT.addTab(self.workerT, "")
        self.administrationT = QtWidgets.QWidget()
        self.administrationT.setObjectName("administrationT")
        self.administrationTable = QtWidgets.QTableWidget(self.administrationT)
        self.administrationTable.setGeometry(QtCore.QRect(0, 0, 541, 551))
        self.administrationTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.administrationTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.administrationTable.setObjectName("administrationTable")
        self.administrationTable.setColumnCount(0)
        self.administrationTable.setRowCount(0)
        self.btnUpdateAdministrationTable = QtWidgets.QPushButton(self.administrationT)
        self.btnUpdateAdministrationTable.setGeometry(QtCore.QRect(560, 10, 211, 23))
        self.btnUpdateAdministrationTable.setObjectName("btnUpdateAdministrationTable")
        self.groupBox_10 = QtWidgets.QGroupBox(self.administrationT)
        self.groupBox_10.setGeometry(QtCore.QRect(550, 240, 231, 80))
        self.groupBox_10.setObjectName("groupBox_10")
        self.cbAdministrationDelete = QtWidgets.QComboBox(self.groupBox_10)
        self.cbAdministrationDelete.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbAdministrationDelete.setObjectName("cbAdministrationDelete")
        self.btnDeleteAdministration = QtWidgets.QPushButton(self.groupBox_10)
        self.btnDeleteAdministration.setGeometry(QtCore.QRect(10, 50, 211, 23))
        self.btnDeleteAdministration.setObjectName("btnDeleteAdministration")
        self.groupBox_11 = QtWidgets.QGroupBox(self.administrationT)
        self.groupBox_11.setGeometry(QtCore.QRect(550, 50, 231, 71))
        self.groupBox_11.setObjectName("groupBox_11")
        self.btnAddAdministration = QtWidgets.QPushButton(self.groupBox_11)
        self.btnAddAdministration.setGeometry(QtCore.QRect(10, 40, 211, 23))
        self.btnAddAdministration.setObjectName("btnAddAdministration")
        self.administrationNameAdd = QtWidgets.QLineEdit(self.groupBox_11)
        self.administrationNameAdd.setGeometry(QtCore.QRect(10, 20, 211, 20))
        self.administrationNameAdd.setObjectName("administrationNameAdd")
        self.groupBox_12 = QtWidgets.QGroupBox(self.administrationT)
        self.groupBox_12.setGeometry(QtCore.QRect(550, 130, 231, 101))
        self.groupBox_12.setObjectName("groupBox_12")
        self.administrationNameUpdate = QtWidgets.QLineEdit(self.groupBox_12)
        self.administrationNameUpdate.setGeometry(QtCore.QRect(10, 50, 211, 20))
        self.administrationNameUpdate.setObjectName("administrationNameUpdate")
        self.btnUpdateAdministration = QtWidgets.QPushButton(self.groupBox_12)
        self.btnUpdateAdministration.setGeometry(QtCore.QRect(10, 70, 211, 23))
        self.btnUpdateAdministration.setObjectName("btnUpdateAdministration")
        self.cbAdministrationUpdate = QtWidgets.QComboBox(self.groupBox_12)
        self.cbAdministrationUpdate.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbAdministrationUpdate.setObjectName("cbAdministrationUpdate")
        self.leadirshipT.addTab(self.administrationT, "")
        self.departmentT = QtWidgets.QWidget()
        self.departmentT.setObjectName("departmentT")
        self.departmentTable = QtWidgets.QTableWidget(self.departmentT)
        self.departmentTable.setGeometry(QtCore.QRect(0, 0, 541, 551))
        self.departmentTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.departmentTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.departmentTable.setObjectName("departmentTable")
        self.departmentTable.setColumnCount(0)
        self.departmentTable.setRowCount(0)
        self.groupBox_7 = QtWidgets.QGroupBox(self.departmentT)
        self.groupBox_7.setGeometry(QtCore.QRect(550, 50, 231, 71))
        self.groupBox_7.setObjectName("groupBox_7")
        self.btnAddDepartment = QtWidgets.QPushButton(self.groupBox_7)
        self.btnAddDepartment.setGeometry(QtCore.QRect(10, 40, 211, 23))
        self.btnAddDepartment.setObjectName("btnAddDepartment")
        self.departmentNameAdd = QtWidgets.QLineEdit(self.groupBox_7)
        self.departmentNameAdd.setGeometry(QtCore.QRect(10, 20, 211, 20))
        self.departmentNameAdd.setObjectName("departmentNameAdd")
        self.btnUpdateDepartmentTable = QtWidgets.QPushButton(self.departmentT)
        self.btnUpdateDepartmentTable.setGeometry(QtCore.QRect(560, 10, 211, 23))
        self.btnUpdateDepartmentTable.setObjectName("btnUpdateDepartmentTable")
        self.groupBox_8 = QtWidgets.QGroupBox(self.departmentT)
        self.groupBox_8.setGeometry(QtCore.QRect(550, 240, 231, 80))
        self.groupBox_8.setObjectName("groupBox_8")
        self.cbDepartmentDelete = QtWidgets.QComboBox(self.groupBox_8)
        self.cbDepartmentDelete.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbDepartmentDelete.setObjectName("cbDepartmentDelete")
        self.btnDeleteDepartment = QtWidgets.QPushButton(self.groupBox_8)
        self.btnDeleteDepartment.setGeometry(QtCore.QRect(10, 50, 211, 23))
        self.btnDeleteDepartment.setObjectName("btnDeleteDepartment")
        self.groupBox_9 = QtWidgets.QGroupBox(self.departmentT)
        self.groupBox_9.setGeometry(QtCore.QRect(550, 130, 231, 101))
        self.groupBox_9.setObjectName("groupBox_9")
        self.departmentNameUpdate = QtWidgets.QLineEdit(self.groupBox_9)
        self.departmentNameUpdate.setGeometry(QtCore.QRect(10, 50, 211, 20))
        self.departmentNameUpdate.setObjectName("departmentNameUpdate")
        self.btnUpdateDepartment = QtWidgets.QPushButton(self.groupBox_9)
        self.btnUpdateDepartment.setGeometry(QtCore.QRect(10, 70, 211, 23))
        self.btnUpdateDepartment.setObjectName("btnUpdateDepartment")
        self.cbDepartmentUpdate = QtWidgets.QComboBox(self.groupBox_9)
        self.cbDepartmentUpdate.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbDepartmentUpdate.setObjectName("cbDepartmentUpdate")
        self.leadirshipT.addTab(self.departmentT, "")
        self.leadershipT = QtWidgets.QWidget()
        self.leadershipT.setObjectName("leadershipT")
        self.leadershipTable = QtWidgets.QTableWidget(self.leadershipT)
        self.leadershipTable.setGeometry(QtCore.QRect(0, 0, 541, 551))
        self.leadershipTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.leadershipTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.leadershipTable.setObjectName("leadershipTable")
        self.leadershipTable.setColumnCount(0)
        self.leadershipTable.setRowCount(0)
        self.groupBox_6 = QtWidgets.QGroupBox(self.leadershipT)
        self.groupBox_6.setGeometry(QtCore.QRect(550, 130, 231, 101))
        self.groupBox_6.setObjectName("groupBox_6")
        self.leadershipNameUpdate = QtWidgets.QLineEdit(self.groupBox_6)
        self.leadershipNameUpdate.setGeometry(QtCore.QRect(10, 50, 211, 20))
        self.leadershipNameUpdate.setObjectName("leadershipNameUpdate")
        self.btnUpdateLeadership = QtWidgets.QPushButton(self.groupBox_6)
        self.btnUpdateLeadership.setGeometry(QtCore.QRect(10, 70, 211, 23))
        self.btnUpdateLeadership.setObjectName("btnUpdateLeadership")
        self.cbLeadershipUpdate = QtWidgets.QComboBox(self.groupBox_6)
        self.cbLeadershipUpdate.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbLeadershipUpdate.setObjectName("cbLeadershipUpdate")
        self.groupBox_4 = QtWidgets.QGroupBox(self.leadershipT)
        self.groupBox_4.setGeometry(QtCore.QRect(550, 50, 231, 71))
        self.groupBox_4.setObjectName("groupBox_4")
        self.btnAddLeadership = QtWidgets.QPushButton(self.groupBox_4)
        self.btnAddLeadership.setGeometry(QtCore.QRect(10, 40, 211, 23))
        self.btnAddLeadership.setObjectName("btnAddLeadership")
        self.leadershipNameAdd = QtWidgets.QLineEdit(self.groupBox_4)
        self.leadershipNameAdd.setGeometry(QtCore.QRect(10, 20, 211, 20))
        self.leadershipNameAdd.setObjectName("leadershipNameAdd")
        self.btnUpdateLeadershipTable = QtWidgets.QPushButton(self.leadershipT)
        self.btnUpdateLeadershipTable.setGeometry(QtCore.QRect(560, 10, 211, 23))
        self.btnUpdateLeadershipTable.setObjectName("btnUpdateLeadershipTable")
        self.groupBox_5 = QtWidgets.QGroupBox(self.leadershipT)
        self.groupBox_5.setGeometry(QtCore.QRect(550, 240, 231, 80))
        self.groupBox_5.setObjectName("groupBox_5")
        self.cbLeadershipDelete = QtWidgets.QComboBox(self.groupBox_5)
        self.cbLeadershipDelete.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbLeadershipDelete.setObjectName("cbLeadershipDelete")
        self.btnDeleteLeadership = QtWidgets.QPushButton(self.groupBox_5)
        self.btnDeleteLeadership.setGeometry(QtCore.QRect(10, 50, 211, 23))
        self.btnDeleteLeadership.setObjectName("btnDeleteLeadership")
        self.leadirshipT.addTab(self.leadershipT, "")
        self.noteT = QtWidgets.QWidget()
        self.noteT.setObjectName("noteT")
        self.noteTable = QtWidgets.QTableWidget(self.noteT)
        self.noteTable.setGeometry(QtCore.QRect(0, 0, 541, 551))
        self.noteTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.noteTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.noteTable.setColumnCount(0)
        self.noteTable.setObjectName("noteTable")
        self.noteTable.setRowCount(0)
        self.btnUpdateNoteTable = QtWidgets.QPushButton(self.noteT)
        self.btnUpdateNoteTable.setGeometry(QtCore.QRect(560, 10, 211, 23))
        self.btnUpdateNoteTable.setObjectName("btnUpdateNoteTable")
        self.groupBox = QtWidgets.QGroupBox(self.noteT)
        self.groupBox.setGeometry(QtCore.QRect(550, 50, 231, 71))
        self.groupBox.setObjectName("groupBox")
        self.btnAddNote = QtWidgets.QPushButton(self.groupBox)
        self.btnAddNote.setGeometry(QtCore.QRect(10, 40, 211, 23))
        self.btnAddNote.setObjectName("btnAddNote")
        self.noteNameAdd = QtWidgets.QLineEdit(self.groupBox)
        self.noteNameAdd.setGeometry(QtCore.QRect(10, 20, 211, 20))
        self.noteNameAdd.setObjectName("noteNameAdd")
        self.groupBox_2 = QtWidgets.QGroupBox(self.noteT)
        self.groupBox_2.setGeometry(QtCore.QRect(550, 130, 231, 101))
        self.groupBox_2.setObjectName("groupBox_2")
        self.noteNameUpdate = QtWidgets.QLineEdit(self.groupBox_2)
        self.noteNameUpdate.setGeometry(QtCore.QRect(10, 50, 211, 20))
        self.noteNameUpdate.setObjectName("noteNameUpdate")
        self.btnUpdateNote = QtWidgets.QPushButton(self.groupBox_2)
        self.btnUpdateNote.setGeometry(QtCore.QRect(10, 70, 211, 23))
        self.btnUpdateNote.setObjectName("btnUpdateNote")
        self.cbNoteUpdate = QtWidgets.QComboBox(self.groupBox_2)
        self.cbNoteUpdate.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbNoteUpdate.setObjectName("cbNoteUpdate")
        self.groupBox_3 = QtWidgets.QGroupBox(self.noteT)
        self.groupBox_3.setGeometry(QtCore.QRect(550, 240, 231, 80))
        self.groupBox_3.setObjectName("groupBox_3")
        self.cbNoteDelete = QtWidgets.QComboBox(self.groupBox_3)
        self.cbNoteDelete.setGeometry(QtCore.QRect(10, 20, 211, 22))
        self.cbNoteDelete.setObjectName("cbNoteDelete")
        self.btnDeleteNote = QtWidgets.QPushButton(self.groupBox_3)
        self.btnDeleteNote.setGeometry(QtCore.QRect(10, 50, 211, 23))
        self.btnDeleteNote.setObjectName("btnDeleteNote")
        self.leadirshipT.addTab(self.noteT, "")
        self.tabWidget.addTab(self.adminT, "")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        self.leadirshipT.setCurrentIndex(0)
        self.toolBox_2.setCurrentIndex(0)
        self.toolBox.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Выбор отпусков v0.3"))
        self.btnSearch.setText(_translate("MainWindow", "Найти"))
        self.vacationSelectTable.setSortingEnabled(True)
        self.label_21.setText(_translate("MainWindow", "Месяц"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.selectT), _translate("MainWindow", "Выборка"))
        self.vacationTable.setSortingEnabled(True)
        self.groupBox_16.setTitle(_translate("MainWindow", "Добавить работника"))
        self.btnAddVacation.setText(_translate("MainWindow", "Добавить"))
        self.label_11.setText(_translate("MainWindow", "Работник"))
        self.label_12.setText(_translate("MainWindow", "Запланированная дата"))
        self.label_13.setText(_translate("MainWindow", "Фактическая дата"))
        self.label_14.setText(_translate("MainWindow", "Количество календарных дней"))
        self.label_15.setText(_translate("MainWindow", "Примечания"))
        self.toolBox_2.setItemText(self.toolBox_2.indexOf(self.page_4), _translate("MainWindow", "Добавить отпуск"))
        self.groupBox_17.setTitle(_translate("MainWindow", "Обновить отпуск"))
        self.btnUpdateVacation.setText(_translate("MainWindow", "Обновить"))
        self.label_20.setText(_translate("MainWindow", "Запланированная дата"))
        self.label_19.setText(_translate("MainWindow", "Примечания"))
        self.label_18.setText(_translate("MainWindow", "Количество календарных дней"))
        self.label_16.setText(_translate("MainWindow", "Работник"))
        self.label_17.setText(_translate("MainWindow", "Фактическая дата"))
        self.toolBox_2.setItemText(self.toolBox_2.indexOf(self.page_5), _translate("MainWindow", "Обновить отпуск"))
        self.groupBox_18.setTitle(_translate("MainWindow", "Удалить отпуск"))
        self.btnDeleteVacation.setText(_translate("MainWindow", "Удалить"))
        self.toolBox_2.setItemText(self.toolBox_2.indexOf(self.page_6), _translate("MainWindow", "Удалить отпуск"))
        self.btnUpdateVacationTable.setText(_translate("MainWindow", "Обновить таблицу"))
        self.leadirshipT.setTabText(self.leadirshipT.indexOf(self.vacationT), _translate("MainWindow", "Отпуска"))
        self.workerTable.setSortingEnabled(True)
        self.btnUpdateWorkerTable.setText(_translate("MainWindow", "Обновить таблицу"))
        self.groupBox_15.setTitle(_translate("MainWindow", "Добавить работника"))
        self.btnAddWorker.setText(_translate("MainWindow", "Добавить"))
        self.label.setText(_translate("MainWindow", "ФИО"))
        self.label_2.setText(_translate("MainWindow", "Дата рождения"))
        self.label_3.setText(_translate("MainWindow", "Управление"))
        self.label_4.setText(_translate("MainWindow", "Отдел"))
        self.label_5.setText(_translate("MainWindow", "Руководство"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page), _translate("MainWindow", "Добавить работника"))
        self.groupBox_14.setTitle(_translate("MainWindow", "Обновить работника"))
        self.label_6.setText(_translate("MainWindow", "ФИО"))
        self.label_7.setText(_translate("MainWindow", "Год рождения"))
        self.btnUpdateWorker.setText(_translate("MainWindow", "Обновить"))
        self.label_8.setText(_translate("MainWindow", "Руководство"))
        self.label_9.setText(_translate("MainWindow", "Управление"))
        self.label_10.setText(_translate("MainWindow", "Отдел"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page_2), _translate("MainWindow", "Обновить работника"))
        self.groupBox_13.setTitle(_translate("MainWindow", "Удалить работника"))
        self.btnDeleteWorker.setText(_translate("MainWindow", "Удалить"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page_3), _translate("MainWindow", "Удалить работника"))
        self.btnWorkerUpPos.setText(_translate("MainWindow", "Вверх"))
        self.btnWorkerDownPos.setText(_translate("MainWindow", "Вниз"))
        self.leadirshipT.setTabText(self.leadirshipT.indexOf(self.workerT), _translate("MainWindow", "Работники"))
        self.administrationTable.setSortingEnabled(True)
        self.btnUpdateAdministrationTable.setText(_translate("MainWindow", "Обновить таблицу"))
        self.groupBox_10.setTitle(_translate("MainWindow", "Удалить управление"))
        self.btnDeleteAdministration.setText(_translate("MainWindow", "Удалить"))
        self.groupBox_11.setTitle(_translate("MainWindow", "Добавить управление"))
        self.btnAddAdministration.setText(_translate("MainWindow", "Добавить"))
        self.groupBox_12.setTitle(_translate("MainWindow", "Обновить управление"))
        self.btnUpdateAdministration.setText(_translate("MainWindow", "Обновить"))
        self.leadirshipT.setTabText(self.leadirshipT.indexOf(self.administrationT), _translate("MainWindow", "Управления"))
        self.departmentTable.setSortingEnabled(True)
        self.groupBox_7.setTitle(_translate("MainWindow", "Добавить отдел"))
        self.btnAddDepartment.setText(_translate("MainWindow", "Добавить"))
        self.btnUpdateDepartmentTable.setText(_translate("MainWindow", "Обновить таблицу"))
        self.groupBox_8.setTitle(_translate("MainWindow", "Удалить отдел"))
        self.btnDeleteDepartment.setText(_translate("MainWindow", "Удалить"))
        self.groupBox_9.setTitle(_translate("MainWindow", "Обновить отдел"))
        self.btnUpdateDepartment.setText(_translate("MainWindow", "Обновить"))
        self.leadirshipT.setTabText(self.leadirshipT.indexOf(self.departmentT), _translate("MainWindow", "Отделы"))
        self.leadershipTable.setSortingEnabled(True)
        self.groupBox_6.setTitle(_translate("MainWindow", "Обновить руководство"))
        self.btnUpdateLeadership.setText(_translate("MainWindow", "Обновить"))
        self.groupBox_4.setTitle(_translate("MainWindow", "Добавить руководство"))
        self.btnAddLeadership.setText(_translate("MainWindow", "Добавить"))
        self.btnUpdateLeadershipTable.setText(_translate("MainWindow", "Обновить таблицу"))
        self.groupBox_5.setTitle(_translate("MainWindow", "Удалить руководство"))
        self.btnDeleteLeadership.setText(_translate("MainWindow", "Удалить"))
        self.leadirshipT.setTabText(self.leadirshipT.indexOf(self.leadershipT), _translate("MainWindow", "Руководства"))
        self.noteTable.setSortingEnabled(True)
        self.btnUpdateNoteTable.setText(_translate("MainWindow", "Обновить таблицу"))
        self.groupBox.setTitle(_translate("MainWindow", "Добавить примечание"))
        self.btnAddNote.setText(_translate("MainWindow", "Добавить"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Обновить примечание"))
        self.btnUpdateNote.setText(_translate("MainWindow", "Обновить"))
        self.groupBox_3.setTitle(_translate("MainWindow", "Удалить примечание"))
        self.btnDeleteNote.setText(_translate("MainWindow", "Удалить"))
        self.leadirshipT.setTabText(self.leadirshipT.indexOf(self.noteT), _translate("MainWindow", "Примечания"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.adminT), _translate("MainWindow", "Администрирование"))

