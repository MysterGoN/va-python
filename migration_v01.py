from playhouse.migrate import SqliteMigrator, migrate
from peewee import (SqliteDatabase, IntegerField, SQL)

from database import Worker

db = SqliteDatabase('database/main.db', pragmas={
    'journal_mode': 'wal',
    'foreign_keys': 1,
})


def migrate_v01():
    workers = list(db.execute_sql('SELECT * FROM worker'))

    migrator = SqliteMigrator(db)
    position_field = IntegerField(unique=True, null=True,
                                  constraints=[SQL('AUTO_INCREMENT')])
    with db.atomic():
        migrate(
            migrator.add_column('Worker', 'position', position_field)
        )

    for i in range(len(workers)):
        query = Worker.update(position=i+1).where(Worker.id == workers[i][0])
        query.execute()


if __name__ == '__main__':
    migrate_v01()
