import sys
import traceback

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QIntValidator
from PyQt5.QtWidgets import QTableWidgetItem, QListWidgetItem

from database import (
    Administration,
    Department,
    Leadership,
    Note,
    Vacation,
    Worker,
    db
)
from peewee import fn
from datetime import datetime

from design import design


# **********************************************************
# **********************  DECORATORS  **********************
# **********************************************************

def sort_fix(item):
    def wrap(f):
        def wrapped_f(self):
            self.__getitem__(item).clearSelection()
            self.__getitem__(item).setSortingEnabled(False)
            f(self)
            self.__getitem__(item).setSortingEnabled(True)

        return wrapped_f

    return wrap


# **********************************************************
# **********************  FUNCTIONS  ***********************
# **********************************************************


def select_cb(cb, text):
    index = cb.findText(text)
    if index != -1:
        cb.setCurrentIndex(index)


def get_date_from_string(string):
    return datetime.strptime(string, '%d.%m.%Y').date()


class ExampleApp(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.init_search()
        self.init_vacations()
        self.init_workers()
        self.init_administrations()
        self.init_departments()
        self.init_leaderships()
        self.init_notes()

    def __getitem__(self, item):
        return getattr(self, item)

    # **********************************************************
    # ************************  SEARCH  ************************
    # **********************************************************

    def init_search(self):
        months = {
            'Январь': 1,
            'Февраль': 2,
            'Март': 3,
            'Апрель': 4,
            'Май': 5,
            'Июнь': 6,
            'Июль': 7,
            'Август': 8,
            'Сентябрь': 9,
            'Октябрь': 10,
            'Ноябрь': 11,
            'Декабрь': 12,
        }

        for k, v in months.items():
            self.cbMonth.addItem(k, v)

        # Инициализация кнопок
        self.btnSearch.clicked.connect(self.search_vacations)

        # Инициализация таблицы
        self.vacationSelectTable.setColumnCount(5)
        col_title = ["Работник", "Дата рождения", "Запланированная дата",
                     "Кол-во календарных дней", "Примечания"]
        self.vacationSelectTable.setHorizontalHeaderLabels(col_title)
        self.vacationSelectTable.setEditTriggers(
            QtWidgets.QTableWidget.NoEditTriggers)

    @sort_fix('vacationSelectTable')
    def search_vacations(self):
        query = (Vacation
                 .select()
                 .where(fn.date_part('month',
                                     Vacation.scheduledDate) == self.cbMonth.currentData()))

        self.vacationSelectTable.setRowCount(len(query))
        row = 0
        for item in query:
            self.vacationSelectTable.setItem(row, 0, QTableWidgetItem(
                item.workerId.fullName))
            self.vacationSelectTable.setItem(row, 1, QTableWidgetItem(
                f"{item.workerId.birthday:%d.%m.%Y}"))
            self.vacationSelectTable.setItem(row, 2, QTableWidgetItem(
                f"{item.scheduledDate:%d.%m.%Y}"))
            self.vacationSelectTable.setItem(row, 3, QCustomTableWidgetItem(
                str(item.numberOfDays)))
            self.vacationSelectTable.setItem(row, 4, QTableWidgetItem(
                ' + '.join([x.name for x in item.notes])))
            row += 1

    # **********************************************************
    # ***********************  VACATION  ***********************
    # **********************************************************

    def add_vacation(self):
        try:
            notes = [x.data(1) for x in self.lwVacationAdd.selectedItems()]
            data = {
                'workerId': self.cbVacationWorkerAdd.currentData(),
                'scheduledDate': self.deVacationSheduleAdd.date().toPyDate(),
                'actualDate': self.deVacationActualAdd.date().toPyDate(),
                'numberOfDays': self.vacationNumberOfDaysAdd.text(),
            }

            item = Vacation.create(**data)
            item.notes = notes
            item.save()

            self.vacationNumberOfDaysAdd.setText('')

            self.update_vacation_tab()
        except Exception as e:
            traceback.print_exc()

    def update_vacation(self):
        try:
            notes = [x.data(1) for x in self.lwVacationUpdate.selectedItems()]
            data = {
                'workerId': self.cbVacationWorkerUpdate.currentData(),
                'scheduledDate': self.deVacationSheduleUpdate.date().toPyDate(),
                'actualDate': self.deVacationActualUpdate.date().toPyDate(),
                'numberOfDays': self.vacationNumberOfDaysUpdate.text(),
            }
            query = Vacation.update(**data) \
                .where(Vacation.id == int(self.cbVacationUpdate.currentText()))
            query.execute()

            item = Vacation.get_by_id(int(self.cbVacationUpdate.currentText()))
            item.notes = notes
            item.save()

            self.vacationNumberOfDaysUpdate.setText('')

            self.update_vacation_tab()
        except Exception as e:
            traceback.print_exc()

    def delete_vacation(self):
        try:
            query = Vacation.delete().where(
                Vacation.id == int(self.cbVacationDelete.currentText()))
            query.execute()

            self.update_vacation_tab()
        except Exception as e:
            traceback.print_exc()

    def update_vacation_tab(self):
        self.update_vacation_table()
        self.update_vacation_comboboxes()

    @sort_fix('vacationTable')
    def update_vacation_table(self):
        vacations = Vacation.select()
        self.vacationTable.setRowCount(len(vacations))
        row = 0
        for vacation in vacations:
            self.vacationTable.setItem(row, 0, QCustomTableWidgetItem(
                str(vacation.id)))
            self.vacationTable.setItem(row, 1, QTableWidgetItem(
                vacation.workerId.fullName))
            self.vacationTable.setItem(row, 2, QTableWidgetItem(
                f"{vacation.scheduledDate:%d.%m.%Y}"))
            self.vacationTable.setItem(row, 3, QTableWidgetItem(
                f"{vacation.actualDate:%d.%m.%Y}"))
            self.vacationTable.setItem(row, 4, QCustomTableWidgetItem(
                str(vacation.numberOfDays)))
            item = QTableWidgetItem(
                ' + '.join([x.name for x in vacation.notes]))
            item.setData(1, vacation.notes)
            self.vacationTable.setItem(row, 5, item)
            row += 1

    def update_vacation_comboboxes(self):
        self.cbVacationUpdate.clear()
        self.cbVacationDelete.clear()

        add_current = self.cbVacationWorkerAdd.currentIndex()
        self.cbVacationWorkerAdd.clear()
        self.lwVacationAdd.clear()

        self.cbVacationWorkerUpdate.clear()
        self.lwVacationUpdate.clear()

        vacations = [str(x.id) for x in Vacation.select()]
        self.cbVacationUpdate.addItems(vacations)
        self.cbVacationDelete.addItems(vacations)

        for worker in Worker.select():
            self.cbVacationWorkerAdd.addItem(worker.fullName, worker)
            self.cbVacationWorkerUpdate.addItem(worker.fullName, worker)
        self.cbVacationWorkerAdd.setCurrentIndex(add_current)
        for note in Note.select():
            item = QListWidgetItem(note.name)
            item.setData(1, note)
            self.lwVacationAdd.addItem(item.clone())
            self.lwVacationUpdate.addItem(item.clone())

    def vacation_row_to_update(self, row, col):
        try:
            c1 = self.vacationTable.item(row, 0).text()
            c2 = self.vacationTable.item(row, 1).text()
            c3 = self.vacationTable.item(row, 2).text()
            c4 = self.vacationTable.item(row, 3).text()
            c5 = self.vacationTable.item(row, 4).text()
            c6 = list(self.vacationTable.item(row, 5).data(1))

            select_cb(self.cbVacationUpdate, c1)
            select_cb(self.cbVacationWorkerUpdate, c2)
            self.deVacationSheduleUpdate.setDate(get_date_from_string(c3))
            self.deVacationActualUpdate.setDate(get_date_from_string(c4))
            self.vacationNumberOfDaysUpdate.setText(c5)

            self.lwVacationUpdate.clearSelection()
            for i in range(len(self.lwVacationUpdate)):
                item = self.lwVacationUpdate.item(i)
                if item.data(1) in c6:
                    item.setSelected(True)

        except Exception as e:
            traceback.print_exc()

    def init_vacations(self):
        # Инициализация таблицы
        self.vacationTable.setColumnCount(6)
        col_title = ["ID", "Работник", "Запланированная дата",
                     "Фактическая дата", "Кол-во календарных дней",
                     "Примечания"]
        self.vacationTable.setHorizontalHeaderLabels(col_title)
        self.vacationTable.setEditTriggers(
            QtWidgets.QTableWidget.NoEditTriggers)
        self.vacationTable.cellClicked.connect(self.vacation_row_to_update)

        # Инициализация кнопок
        self.btnAddVacation.clicked.connect(self.add_vacation)
        self.btnUpdateVacation.clicked.connect(self.update_vacation)
        self.btnDeleteVacation.clicked.connect(self.delete_vacation)
        self.btnUpdateVacationTable.clicked.connect(self.update_vacation_table)

        # Добавляем валидацию
        self.vacationNumberOfDaysAdd.setValidator(QIntValidator())
        self.vacationNumberOfDaysUpdate.setValidator(QIntValidator())

        # Обновляем данные таблицы
        self.update_vacation_table()
        self.update_vacation_comboboxes()

    # **********************************************************
    # ************************  WORKER  ************************
    # **********************************************************

    def add_worker(self):
        try:
            data = {
                'fullName': self.workerNameAdd.text(),
                'birthday': self.deWorkerAdd.date().toPyDate(),
                'administrationId': self.cbWorkerAdministrationAdd.currentData(),
                'departmentId': self.cbWorkerDepartmentAdd.currentData(),
                'leadershipId': self.cbWorkerLeadershipAdd.currentData(),
            }

            Worker.create(**data)
            self.workerNameAdd.setText('')

            self.update_worker_tab()
            self.update_vacation_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_worker(self):
        try:
            data = {
                'fullName': self.workerNameUpdate.text(),
                'birthday': self.deWorkerUpdate.date().toPyDate(),
                'administrationId': self.cbWorkerAdministrationUpdate.currentData(),
                'departmentId': self.cbWorkerDepartmentUpdate.currentData(),
                'leadershipId': self.cbWorkerLeadershipUpdate.currentData(),
            }

            query = Worker.update(**data) \
                .where(Worker.id == int(self.cbWorkerUpdate.currentText()))
            query.execute()
            self.workerNameUpdate.setText('')

            self.update_worker_tab()
            self.update_vacation_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def delete_worker(self):
        try:
            query = Worker.delete().where(
                Worker.id == int(self.cbWorkerDelete.currentText()))
            query.execute()

            self.update_worker_tab()
            self.update_vacation_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_worker_tab(self):
        self.update_worker_table()
        self.update_worker_comboboxes()

    def worker_up_position(self):
        try:
            row_number = self.workerTable.currentRow()
            if row_number == 0:
                return
            prew_number = row_number - 1
            item_id = int(self.workerTable.selectedItems()[0].text())
            self.workerTable.selectRow(prew_number)
            prew_item_id = int(self.workerTable.selectedItems()[0].text())

            with db.transaction():
                worker = Worker.select().where(Worker.id == item_id)[0]
                pos = worker.position
                prew_worker = Worker.select().where(Worker.id == prew_item_id)[
                    0]
                prew_pos = prew_worker.position

                query = (Worker.update(position=None)
                         .where(Worker.id == prew_item_id))
                query.execute()
                query = (Worker.update(position=prew_pos)
                         .where(Worker.id == item_id))
                query.execute()
                query = (Worker.update(position=pos)
                         .where(Worker.id == prew_item_id))
                query.execute()

            self.update_worker_table()
            self.workerTable.selectRow(prew_number)
        except Exception as e:
            traceback.print_exc()

    def worker_down_position(self):
        row_count = self.workerTable.rowCount()
        row_number = self.workerTable.selectedIndexes()[0].row()
        if row_number == row_count - 1:
            return

        next_number = row_number + 1
        item_id = int(self.workerTable.selectedItems()[0].text())
        self.workerTable.selectRow(next_number)
        next_item_id = int(self.workerTable.selectedItems()[0].text())

        with db.transaction():
            worker = Worker.select().where(Worker.id == item_id)[0]
            pos = worker.position
            next_worker = Worker.select().where(Worker.id == next_item_id)[0]
            next_pos = next_worker.position

            query = (Worker.update(position=None)
                     .where(Worker.id == next_item_id))
            query.execute()
            query = (Worker.update(position=next_pos)
                     .where(Worker.id == item_id))
            query.execute()
            query = (Worker.update(position=pos)
                     .where(Worker.id == next_item_id))
            query.execute()

        self.update_worker_table()
        self.workerTable.selectRow(next_number)

    @sort_fix('workerTable')
    def update_worker_table(self):
        workers = Worker.select().order_by(Worker.position)
        self.workerTable.setRowCount(len(workers))
        row = 0
        for worker in workers:
            self.workerTable.setItem(row, 0,
                                     QCustomTableWidgetItem(str(worker.id)))
            self.workerTable.setItem(row, 1, QTableWidgetItem(worker.fullName))
            self.workerTable.setItem(row, 2, QTableWidgetItem(
                f"{worker.birthday:%d.%m.%Y}"))
            self.workerTable.setItem(row, 3, QTableWidgetItem(
                worker.administrationId.name))
            self.workerTable.setItem(row, 4, QTableWidgetItem(
                worker.departmentId.name))
            self.workerTable.setItem(row, 5, QTableWidgetItem(
                worker.leadershipId.name))
            self.workerTable.setItem(row, 6,
                                     QTableWidgetItem(str(worker.position)))
            row += 1

    def update_worker_comboboxes(self):
        self.cbWorkerUpdate.clear()
        self.cbWorkerDelete.clear()

        self.cbWorkerAdministrationAdd.clear()
        self.cbWorkerDepartmentAdd.clear()
        self.cbWorkerLeadershipAdd.clear()

        self.cbWorkerAdministrationUpdate.clear()
        self.cbWorkerDepartmentUpdate.clear()
        self.cbWorkerLeadershipUpdate.clear()

        workers = [str(x.id) for x in Worker.select()]
        self.cbWorkerUpdate.addItems(workers)
        self.cbWorkerDelete.addItems(workers)

        for adm in Administration.select():
            self.cbWorkerAdministrationAdd.addItem(adm.name, adm)
            self.cbWorkerAdministrationUpdate.addItem(adm.name, adm)
        for dep in Department.select():
            self.cbWorkerDepartmentAdd.addItem(dep.name, dep)
            self.cbWorkerDepartmentUpdate.addItem(dep.name, dep)
        for lead in Leadership.select():
            self.cbWorkerLeadershipAdd.addItem(lead.name, lead)
            self.cbWorkerLeadershipUpdate.addItem(lead.name, lead)

    def worker_row_to_update(self, row, col):
        c1 = self.workerTable.item(row, 0).text()
        c2 = self.workerTable.item(row, 1).text()
        c3 = self.workerTable.item(row, 2).text()
        c4 = self.workerTable.item(row, 3).text()
        c5 = self.workerTable.item(row, 4).text()
        c6 = self.workerTable.item(row, 5).text()

        select_cb(self.cbWorkerUpdate, c1)
        self.workerNameUpdate.setText(c2)
        self.deWorkerUpdate.setDate(get_date_from_string(c3))
        select_cb(self.cbWorkerAdministrationUpdate, c4)
        select_cb(self.cbWorkerDepartmentUpdate, c5)
        select_cb(self.cbWorkerLeadershipUpdate, c6)

    def init_workers(self):
        # Инициализация таблицы
        self.workerTable.setColumnCount(7)
        col_title = ["ID", "ФИО", "Дата рождения", "Управление", "Отдел",
                     "Руководство", "Позиция"]
        self.workerTable.setHorizontalHeaderLabels(col_title)
        self.workerTable.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        self.workerTable.cellClicked.connect(self.worker_row_to_update)

        # Инициализация кнопок
        self.btnAddWorker.clicked.connect(self.add_worker)
        self.btnUpdateWorker.clicked.connect(self.update_worker)
        self.btnDeleteWorker.clicked.connect(self.delete_worker)
        self.btnUpdateWorkerTable.clicked.connect(self.update_worker_table)
        self.btnWorkerUpPos.clicked.connect(self.worker_up_position)
        self.btnWorkerDownPos.clicked.connect(self.worker_down_position)

        # Обновляем данные таблицы
        self.update_worker_table()
        self.update_worker_comboboxes()

    # **********************************************************
    # ********************  ADMINISTRATION  ********************
    # **********************************************************

    def add_administration(self):
        try:
            Administration.create(name=self.administrationNameAdd.text())
            self.administrationNameAdd.setText('')

            self.update_administration_tab()
            self.update_worker_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_administration(self):
        try:
            query = Administration.update(
                name=self.administrationNameUpdate.text()) \
                .where(Administration.id == int(
                self.cbAdministrationUpdate.currentText()))
            query.execute()
            self.administrationNameUpdate.setText('')

            self.update_administration_tab()
            self.update_worker_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def delete_administration(self):
        try:
            query = Administration.delete().where(Administration.id == int(
                self.cbAdministrationDelete.currentText()))
            query.execute()

            self.update_administration_tab()
            self.update_worker_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_administration_tab(self):
        self.update_administration_table()
        self.update_administration_comboboxes()

    @sort_fix('administrationTable')
    def update_administration_table(self):
        administrations = Administration.select()
        self.administrationTable.setRowCount(len(administrations))
        row = 0
        for administration in administrations:
            self.administrationTable.setItem(row, 0, QCustomTableWidgetItem(
                str(administration.id)))
            self.administrationTable.setItem(row, 1, QTableWidgetItem(
                administration.name))
            row += 1

    def update_administration_comboboxes(self):
        self.cbAdministrationUpdate.clear()
        self.cbAdministrationDelete.clear()

        administrations = [str(x.id) for x in Administration.select()]
        self.cbAdministrationUpdate.addItems(administrations)
        self.cbAdministrationDelete.addItems(administrations)

    def administration_row_to_update(self, row, col):
        c1 = self.administrationTable.item(row, 0).text()
        c2 = self.administrationTable.item(row, 1).text()

        select_cb(self.cbAdministrationUpdate, c1)
        self.administrationNameUpdate.setText(c2)

    def init_administrations(self):
        # Инициализация таблицы
        self.administrationTable.setColumnCount(2)
        self.administrationTable.setHorizontalHeaderLabels(
            ["ID", "Управление"])
        self.administrationTable.setEditTriggers(
            QtWidgets.QTableWidget.NoEditTriggers)
        self.administrationTable.cellClicked.connect(
            self.administration_row_to_update)

        # Инициализация кнопок
        self.btnAddAdministration.clicked.connect(self.add_administration)
        self.btnUpdateAdministration.clicked.connect(
            self.update_administration)
        self.btnDeleteAdministration.clicked.connect(
            self.delete_administration)
        self.btnUpdateAdministrationTable.clicked.connect(
            self.update_administration_table)

        # Обновляем данные таблицы
        self.update_administration_table()
        self.update_administration_comboboxes()

    # **********************************************************
    # **********************  DEPARTMENT  **********************
    # **********************************************************

    def add_department(self):
        try:
            Department.create(name=self.departmentNameAdd.text())
            self.departmentNameAdd.setText('')

            self.update_department_tab()
            self.update_worker_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_department(self):
        try:
            query = Department.update(name=self.departmentNameUpdate.text()) \
                .where(
                Department.id == int(self.cbDepartmentUpdate.currentText()))
            query.execute()
            self.departmentNameUpdate.setText('')

            self.update_department_tab()
            self.update_worker_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def delete_department(self):
        try:
            query = Department.delete().where(
                Department.id == int(self.cbDepartmentDelete.currentText()))
            query.execute()

            self.update_department_tab()
            self.update_worker_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_department_tab(self):
        self.update_department_table()
        self.update_department_comboboxes()

    @sort_fix('departmentTable')
    def update_department_table(self):
        departments = Department.select()
        self.departmentTable.setRowCount(len(departments))
        row = 0
        for department in departments:
            self.departmentTable.setItem(row, 0, QCustomTableWidgetItem(
                str(department.id)))
            self.departmentTable.setItem(row, 1,
                                         QTableWidgetItem(department.name))
            row += 1

    def update_department_comboboxes(self):
        self.cbDepartmentUpdate.clear()
        self.cbDepartmentDelete.clear()

        departments = [str(x.id) for x in Department.select()]
        self.cbDepartmentUpdate.addItems(departments)
        self.cbDepartmentDelete.addItems(departments)

    def department_row_to_update(self, row, col):
        c1 = self.departmentTable.item(row, 0).text()
        c2 = self.departmentTable.item(row, 1).text()

        select_cb(self.cbDepartmentUpdate, c1)
        self.departmentNameUpdate.setText(c2)

    def init_departments(self):
        # Инициализация таблицы
        self.departmentTable.setColumnCount(2)
        self.departmentTable.setHorizontalHeaderLabels(["ID", "Отдел"])
        self.departmentTable.setEditTriggers(
            QtWidgets.QTableWidget.NoEditTriggers)
        self.departmentTable.cellClicked.connect(self.department_row_to_update)

        # Инициализация кнопок
        self.btnAddDepartment.clicked.connect(self.add_department)
        self.btnUpdateDepartment.clicked.connect(self.update_department)
        self.btnDeleteDepartment.clicked.connect(self.delete_department)
        self.btnUpdateDepartmentTable.clicked.connect(
            self.update_department_table)

        # Обновляем данные таблицы
        self.update_department_table()
        self.update_department_comboboxes()

    # **********************************************************
    # **********************  LEADERSHIP  **********************
    # **********************************************************

    def add_leadership(self):
        try:
            Leadership.create(name=self.leadershipNameAdd.text())
            self.leadershipNameAdd.setText('')

            self.update_leadership_tab()
            self.update_worker_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_leadership(self):
        try:
            query = Leadership.update(name=self.leadershipNameUpdate.text()) \
                .where(
                Leadership.id == int(self.cbLeadershipUpdate.currentText()))
            query.execute()
            self.leadershipNameUpdate.setText('')

            self.update_leadership_tab()
            self.update_worker_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def delete_leadership(self):
        try:
            query = Leadership.delete().where(
                Leadership.id == int(self.cbLeadershipDelete.currentText()))
            query.execute()

            self.update_leadership_tab()
            self.update_worker_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_leadership_tab(self):
        self.update_leadership_table()
        self.update_leadership_comboboxes()

    @sort_fix('leadershipTable')
    def update_leadership_table(self):
        leaderships = Leadership.select()
        self.leadershipTable.setRowCount(len(leaderships))
        row = 0
        for leadership in leaderships:
            self.leadershipTable.setItem(row, 0, QCustomTableWidgetItem(
                str(leadership.id)))
            self.leadershipTable.setItem(row, 1,
                                         QTableWidgetItem(leadership.name))
            row += 1

    def update_leadership_comboboxes(self):
        self.cbLeadershipUpdate.clear()
        self.cbLeadershipDelete.clear()

        leaderships = [str(x.id) for x in Leadership.select()]
        self.cbLeadershipUpdate.addItems(leaderships)
        self.cbLeadershipDelete.addItems(leaderships)

    def leaderships_row_to_update(self, row, col):
        c1 = self.leadershipTable.item(row, 0).text()
        c2 = self.leadershipTable.item(row, 1).text()

        select_cb(self.cbLeadershipUpdate, c1)
        self.leadershipNameUpdate.setText(c2)

    def init_leaderships(self):
        # Инициализация таблицы
        self.leadershipTable.setColumnCount(2)
        self.leadershipTable.setHorizontalHeaderLabels(["ID", "Руководство"])
        self.leadershipTable.setEditTriggers(
            QtWidgets.QTableWidget.NoEditTriggers)
        self.leadershipTable.cellClicked.connect(
            self.leaderships_row_to_update)

        # Инициализация кнопок
        self.btnAddLeadership.clicked.connect(self.add_leadership)
        self.btnUpdateLeadership.clicked.connect(self.update_leadership)
        self.btnDeleteLeadership.clicked.connect(self.delete_leadership)
        self.btnUpdateLeadershipTable.clicked.connect(
            self.update_leadership_table)

        # Обновляем данные таблицы
        self.update_leadership_table()
        self.update_leadership_comboboxes()

    # **********************************************************
    # *************************  NOTE  *************************
    # **********************************************************

    def add_note(self):
        try:
            Note.create(name=self.noteNameAdd.text())
            self.noteNameAdd.setText('')

            self.update_note_tab()
            self.update_vacation_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_note(self):
        try:
            query = Note.update(name=self.noteNameUpdate.text()) \
                .where(Note.id == int(self.cbNoteUpdate.currentText()))
            query.execute()
            self.noteNameUpdate.setText('')

            self.update_note_tab()
            self.update_vacation_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def delete_note(self):
        try:
            query = Note.delete().where(
                Note.id == int(self.cbNoteDelete.currentText()))
            query.execute()

            self.update_note_tab()
            self.update_vacation_comboboxes()
        except Exception as e:
            traceback.print_exc()

    def update_note_tab(self):
        self.update_note_table()
        self.update_note_comboboxes()

    @sort_fix('noteTable')
    def update_note_table(self):
        notes = Note.select()
        self.noteTable.setRowCount(len(notes))
        row = 0
        for note in notes:
            self.noteTable.setItem(row, 0,
                                   QCustomTableWidgetItem(str(note.id)))
            self.noteTable.setItem(row, 1, QTableWidgetItem(note.name))
            row += 1

    def update_note_comboboxes(self):
        self.cbNoteUpdate.clear()
        self.cbNoteDelete.clear()

        notes = [str(x.id) for x in Note.select()]
        self.cbNoteUpdate.addItems(notes)
        self.cbNoteDelete.addItems(notes)

    def note_row_to_update(self, row, col):
        c1 = self.noteTable.item(row, 0).text()
        c2 = self.noteTable.item(row, 1).text()

        select_cb(self.cbNoteUpdate, c1)
        self.noteNameUpdate.setText(c2)

    def init_notes(self):
        # Инициализация таблицы
        self.noteTable.setColumnCount(2)
        self.noteTable.setHorizontalHeaderLabels(["ID", "Примечание"])
        self.noteTable.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        self.noteTable.cellClicked.connect(self.note_row_to_update)

        # Инициализация кнопок
        self.btnAddNote.clicked.connect(self.add_note)
        self.btnUpdateNote.clicked.connect(self.update_note)
        self.btnDeleteNote.clicked.connect(self.delete_note)
        self.btnUpdateNoteTable.clicked.connect(self.update_note_table)

        # Обновляем данные таблицы
        self.update_note_table()
        self.update_note_comboboxes()

    # **********************************************************
    # ********************  OTHER  CLASSES  ********************
    # **********************************************************


class QCustomTableWidgetItem(QTableWidgetItem):
    def __init__(self, value):
        super(QCustomTableWidgetItem, self).__init__(value)

    def __lt__(self, other):
        if isinstance(other, QCustomTableWidgetItem):
            self_data_value = float(self.data(QtCore.Qt.EditRole))
            other_data_value = float(other.data(QtCore.Qt.EditRole))
            return self_data_value < other_data_value
        else:
            return QTableWidgetItem.__lt__(self, other)


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = ExampleApp()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
