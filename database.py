from peewee import (SqliteDatabase, Model, CharField, DateField, fn,
                    ForeignKeyField, IntegerField, ManyToManyField, SQL)
from playhouse.signals import Model as pModel, pre_save

db = SqliteDatabase('database/main.db', pragmas={
    'journal_mode': 'wal',
    'foreign_keys': 1,
})


class Administration(Model):
    name = CharField()

    class Meta:
        database = db


class Department(Model):
    name = CharField()

    class Meta:
        database = db


class Leadership(Model):
    name = CharField()

    class Meta:
        database = db


class Worker(pModel):
    fullName = CharField()
    birthday = DateField()
    administrationId = ForeignKeyField(Administration, on_delete='RESTRICT')
    departmentId = ForeignKeyField(Department, on_delete='RESTRICT')
    leadershipId = ForeignKeyField(Leadership, on_delete='RESTRICT')
    position = IntegerField(unique=True, null=True,
                            constraints=[SQL('AUTO_INCREMENT')])

    class Meta:
        database = db


@pre_save(sender=Worker)
def on_save_handler(model_class, instance, created):
    max = Worker.select(fn.Max(Worker.position)).scalar(as_tuple=True)[0] or 0
    instance.position = max + 1


class Note(Model):
    name = CharField()

    class Meta:
        database = db


class Vacation(Model):
    workerId = ForeignKeyField(Worker, on_delete='RESTRICT')
    scheduledDate = DateField()
    actualDate = DateField()
    numberOfDays = IntegerField()
    notes = ManyToManyField(Note)

    class Meta:
        database = db


if __name__ == '__main__':
    Administration.create_table()
    Department.create_table()
    Leadership.create_table()
    Worker.create_table()
    Note.create_table()
    Vacation.create_table()
    Vacation.notes.get_through_model().create_table()
